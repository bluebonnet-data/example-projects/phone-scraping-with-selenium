from utils import setup, extract_info
import pandas as pd
import argparse
import csv


def main(driver, filename):
    '''
    Loops through all rows in the specified csv and adds the top four phone numbers (w info)
    for each person.

    assumes that the csv includes column names "FN", "LN", "MI", "City", "State"
    '''

    all_new_info = [['Voter File VANID','First name', 'Last name', 'Phone', 'Type', 'Used During', 'Confidence']]
    old_info = pd.read_csv(filename).fillna("")
    for index, row in old_info.iterrows():
        voter_info = {
            'Voter File VANID': row['Voter File VANID'],
            'city': row['City'],
            'state': row['State'],
            'fn': row['FirstName'],
            'mn': row['MiddleName'],
            'ln': row['LastName']
        }
        new_phone_numbers = extract_info(driver, voter_info)
        print(new_phone_numbers)
        all_new_info = all_new_info + new_phone_numbers



    with open("out.csv", "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(all_new_info)


parser = argparse.ArgumentParser(description='Determine which file to lookup info for')
parser.add_argument('filename', type=str, help='Get tweets from')

if __name__ == "__main__":

    args = parser.parse_args()
    if not args.filename:
        parser.error('Set --filename to the filename to perform lookups')
    else:
        print(args.filename)
        driver = setup()
        main(driver, args.filename)
        driver.close()
