# Phone Lookups
This tool uses selenium to automate the process of scraping phone numbers from Been Verified . com.

## Setting up your repo
- To run selenium you need to download a web driver. Instructions & links to drivers can be found [here](https://selenium-python.readthedocs.io/installation.html#drivers). Note that you need to move the driver to your `usr/local/bin` or add the `executable_path=path/to/driver.exe` argument to the call to the driver's constructor (here, `webdriver.Chrome()`.)
- You will also need to create a `.env` file in the repo directory with the BeenVerified username and password. The template for this file is called `env_template`.

# Workflow with VAN
## Export from VAN
1. Create List with the following parameters: 
    - ADD people:
        - Activist Codes: Tier 2, Tier 3, SuperVoters
        - Voter Status & Registration: Check ‘Voter Status’ as ‘Active’
    - REMOVE people:
        - Phones: Go to “Source” and deselect all options
        - Voter Status & Registration: Check ‘Voter Status’ as ‘Active’
2. Click “Run Search”
3. Click “Export”
    -  For “Export Type” select “Standard Text” and choose “Customize Export”
    - For Fields to Export, choose the following:
        - Voter File VANID
        - Mailing Address (Separate Fields)
        - Name
4. Click “Export” then go to “My Export Files”
5. Download Exported file and save in phone-lookup directory
    - Using Saved Lists, bulk export csv with the following voter information
        - VoterId
        - First Name
        - Last Name

## Run the script
- **Van Upload**: Run `bulk_lookup.py` and specify the filename using `-f` (`python3 bulk_lookup.py -f "filename.csv"`)

## Importing back to VAN
1. Filter “Phone” Column for:
    - “No records found (names match but state does not).” 
    - “No exact match”
    - “No records found (no matches)”
2. Delete all filtered rows
3. Move “Voter Van ID” to be the 1st column
4. On Home Page of VAN under “Load Data” select “Run Bulk Uploads”
5. For “Load Data with” select “Voter File ID”
6. On  “Upload a New File” page
    - Mapping Template: “None”
    - File Source: ‘Direct Upload’
    - Select File
7. Click “Enable Advanced Error Handling and Reporting”
8. Under “Apply New Mapping”, select “Apply Phone Numbers”
9. A new menu called “Apply Phone Numbers” will appear
    - Phone*: Select “Choose Column From Data	File” → Select “phone”
    - Phone Type* : Select “Choose Column From Data	File” → Select “type”
    - Don’t touch “Is It Cell”
10. Click “Save Mapping Template”
11. Name the template “missing_numbers” and save
12. Click “Finish"