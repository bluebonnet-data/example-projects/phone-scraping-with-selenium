from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from dotenv import load_dotenv
from urllib.parse import urlencode
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import re
import os
from os import path
import pickle


def setup():
    '''
    Creats a chrome webdriver instance & logs into BeenVerified with Michael's info.
    '''

    driver = webdriver.Chrome()
    driver.get("https://www.beenverified.com/app/login")
    
    if path.exists('cookies.pkl'):

        cookies = pickle.load(open("cookies.pkl", "rb"))
        for cookie in cookies:
            print(cookie)
            driver.add_cookie(cookie)
        driver.get("https://www.beenverified.com/app/dashboard")

    else:

        load_dotenv()
        USERNAME = os.environ["USERNAME"]
        PASSWORD = os.environ["PASSWORD"]

        driver = webdriver.Chrome()
        driver.get("https://www.beenverified.com/app/login")

        username = driver.find_element_by_id("login-email")
        password = driver.find_element_by_id("login-password")

        username.send_keys(USERNAME)
        password.send_keys(PASSWORD)
        driver.find_element_by_id("submit").click()

        pickle.dump(driver.get_cookies(), open("cookies.pkl","wb"))

    wait = WebDriverWait(driver, 600)  # Long wait bc these captchas are HARD
    wait.until(EC.url_to_be("https://www.beenverified.com/app/dashboard"))

    return(driver)


def extract_info(driver, voter_info):
    '''
    Searches for the voter's info in BeenVerified and returns all available info.
    'voter_info' input should be a dict with city, state, fn (first name), mn (middle initial), ln (last name).
    '''

    driver.get("https://www.beenverified.com/app/search/person?{}".format(urlencode(voter_info)))
    try:
        next_button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "link_arrow")))
        voter_not_found = False
    except TimeoutException as ex:
        voter_not_found = True

    info = []
    if voter_not_found:
        info.append([voter_info['Voter File VANID' ], voter_info['fn'], voter_info['ln'], "No records found (no matches)."])
    elif driver.find_elements_by_class_name("not-exact-match"):
        info.append([voter_info['Voter File VANID' ], voter_info['fn'], voter_info['ln'], "No exact match."])
    elif driver.find_element_by_class_name("address").text[-2:] != voter_info['state']:
        print(voter_info['state'])
        print(driver.find_element_by_class_name("address").text[-2:])
        info.append([voter_info['Voter File VANID' ], voter_info['fn'], voter_info['ln'], "No records found (names match but state does not)."])

    else:  # Voter state matches
        next_button.click()
        wait = WebDriverWait(driver, 600)  # Sometimes you hit a capcha here!!
        wait.until(EC.url_contains("bvid="))

        html = BeautifulSoup(driver.page_source, "html.parser")
        phone_number_div = html.find('div', {'id' : 'phone-numbers'})
        cards = phone_number_div.select('div.container.ember-view')
        cards = cards[0:4] if len(cards) > 4 else cards  # only get first 4 numbers

        for card in cards:
            phone_number = card.select('div.modal-header')[0].text.strip() if card.select('div.modal-header') else "Not found"
            phone_type = card.select('div.status-indicator')[0].text.strip() if card.select('div.status-indicator') else "Not found"
            used_during = card.select('div.info')[0].text.strip() if card.select('div.info') else "Not found"
            conf = "High" if card.find(text=re.compile("Higher Confidence")) == "Higher Confidence" else "Low"
            info.append([voter_info['Voter File VANID' ], voter_info['fn'], voter_info['ln'], phone_number, phone_type, used_during, conf])

        if len(cards) == 0:
            info.append([voter_info['Voter File VANID'], voter_info['fn'], voter_info['ln'], "No records found (no phone numbers on file)."])

    return info



